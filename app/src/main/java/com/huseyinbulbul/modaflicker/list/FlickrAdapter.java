package com.huseyinbulbul.modaflicker.list;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.huseyinbulbul.modaflicker.R;
import com.huseyinbulbul.modaflicker.common.Image;
import com.huseyinbulbul.modaflicker.detail.DetailActivity;

import java.util.ArrayList;

/**
 * Created by Hüseyin on 27.8.2016.
 */
public class FlickrAdapter extends RecyclerView.Adapter<FlickrAdapter.FlickerHolder>{
    private ArrayList<Image> images;
    private LinearLayoutManager layoutManager;

    public FlickrAdapter(ArrayList<Image> images, LinearLayoutManager layoutManager){
        this.images=images;
        this.layoutManager=layoutManager;
    }

    @Override
    public FlickerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_flickr, parent, false);
        return new FlickerHolder(view);
    }

    @Override
    public void onBindViewHolder(final FlickerHolder holder, final int position) {
        final Image image=images.get(position);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(holder.imageView.getContext(), DetailActivity.class);
                intent.putExtra(DetailActivity.INDEX, position);
                holder.imageView.getContext().startActivity(intent);
            }
        });
        if(image!=null){
            holder.textView.setText(image.getTitle());
            Glide.with(holder.imageView.getContext()).load(image.getUrlm()).placeholder(R.drawable.placeholder).into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        if(images==null)
            return 0;
        return images.size();
    }

    public class FlickerHolder extends RecyclerView.ViewHolder{
        public ImageView imageView;
        public TextView textView;

        public FlickerHolder(View itemView) {
            super(itemView);
            imageView=(ImageView) itemView.findViewById(R.id.iv_image);
            textView=(TextView) itemView.findViewById(R.id.tv_title);
        }
    }
}

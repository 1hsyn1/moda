package com.huseyinbulbul.modaflicker.list;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.huseyinbulbul.modaflicker.App;
import com.huseyinbulbul.modaflicker.R;
import com.huseyinbulbul.modaflicker.common.Image;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity implements Image.ImagesListener{
    private RelativeLayout loading;
    private RecyclerView recyclerView;
    private FlickrAdapter adapter;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        loading=(RelativeLayout) findViewById(R.id.rl_loading);
        recyclerView=(RecyclerView)findViewById(R.id.rv_images);
        layoutManager=new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        Image.getImages(this, this);
    }

    @Override
    public void imagesReady(ArrayList<Image> images) {
        if(this==null || isFinishing())
            return;
        loading.setVisibility(View.GONE);
        adapter=new FlickrAdapter(images, layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onError() {
        AlertDialog.Builder builder=new AlertDialog.Builder(this)
                .setTitle(R.string.warning)
                .setMessage(R.string.error_description)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        builder.show();
    }
}

package com.huseyinbulbul.modaflicker.detail;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.huseyinbulbul.modaflicker.R;
import com.huseyinbulbul.modaflicker.common.Image;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {
    public static final String INDEX="index";
    private TextView titleView, ownerView, descriptionView, dateView;
    private ImageView imageView;
    private Image image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        titleView=(TextView) findViewById(R.id.tv_title);
        ownerView=(TextView) findViewById(R.id.tv_owner);
        descriptionView=(TextView) findViewById(R.id.tv_description);
        dateView=(TextView) findViewById(R.id.tv_date);
        imageView=(ImageView) findViewById(R.id.iv_image);

        (findViewById(R.id.iv_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        image=Image.getImage(getIntent().getIntExtra(INDEX,-1));
        image.getInfo(new Image.ImageListener() {
            @Override
            public void imageReady() {
                writeImage();
            }

            @Override
            public void onError() {

            }
        });
    }

    private void writeImage(){
        if(this==null || isFinishing())
            return;
        Glide.with(this).load(image.getUrll()).asBitmap().into(imageView);
        titleView.setText(image.getTitle());
        descriptionView.setText(image.getDescription());
        ownerView.setText(image.getOwner());
        dateView.setText(image.getDate());
    }
}

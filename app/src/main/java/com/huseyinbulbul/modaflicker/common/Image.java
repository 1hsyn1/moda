package com.huseyinbulbul.modaflicker.common;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.huseyinbulbul.modaflicker.App;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Hüseyin on 27.8.2016.
 * I followed MVPish structure. I removed P there is just M and V.
 * I think for small projects like this MVP not making anything better.
 */
public class Image {
    public static final String PHOTO="photo";
    public static final String PHOTOS="photos";
    public static final String OWNER="owner";
    public static final String REAL_NAME="realname";
    public static final String TITLE="title";
    public static final String CONTENT="_content";
    public static final String DESCRIPTION="description";
    public static final String DATES="dates";
    public static final String TAKEN="taken";
    public static final String URLM="url_m";
    public static final String URLN="url_n";
    public static final String URLL="url_l";
    public static final String ID="id";
    private static ArrayList<Image> images;
    private static ImagesListener listener;

    private String title="";
    private String owner="";
    private String description="";
    private String date="";
    private String urlm="";
    private String urln="";
    private String urll="";
    private String id="";

    public Image(){}
    public Image(JSONObject object){
        try {
                title=object.optString(TITLE);
                urlm=object.optString(URLM);
                urln=object.optString(URLN);
                urll=object.optString(URLL);
                id=object.optString(ID);
        }catch (Exception e){

        }
    }

    public void getInfo(final ImageListener listener){
        JsonObjectRequest req=new JsonObjectRequest(Request.Method.GET, ApiReqManager.getInfoUrl(id), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject object) {
                JSONObject obj = object.optJSONObject(PHOTO);
                if(obj!=null) {
                    owner = obj.optJSONObject(OWNER).optString(REAL_NAME);
                    description = obj.optJSONObject(DESCRIPTION).optString(CONTENT);
                    date = obj.optJSONObject(DATES).optString(TAKEN);
                    listener.imageReady();
                }
                listener.onError();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError();
            }
        });
        App.getInstance().addToRequestQueue(req);
    }

    public String getTitle() {
        return title;
    }

    public String getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public String getUrlm() {
        return urlm;
    }

    public String getUrln() {
        return urln;
    }

    public String getUrll() {
        return urll;
    }

    public String getId() {
        return id;
    }

    public static void getImages(Context context, ImagesListener listener){
        if(images!=null){
            listener.imagesReady(images);
            return;
        }
        Image.listener=listener;
        images=new ArrayList<>();
        JsonObjectRequest req=new JsonObjectRequest(Request.Method.GET, ApiReqManager.getSearchUrl(), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray array=response.optJSONObject(PHOTOS).optJSONArray(PHOTO);
                if(array==null) {
                    Image.listener.onError();
                    return;
                }
                for(int i=0;i<array.length();i++){
                    Image image=new Image(array.optJSONObject(i));
                    images.add(image);
                }
                Image.listener.imagesReady(images);
                return;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Image.listener.onError();
            }
        });
        App.getInstance().addToRequestQueue(req);
    }

    public static Image getImage(int index){
        if(images==null && index>-1 && index<images.size())
            return null;
        return images.get(index);
    }

    public interface ImagesListener{
        void imagesReady(ArrayList<Image> images);
        void onError();
    }

    public interface ImageListener{
        void imageReady();
        void onError();
    }
}

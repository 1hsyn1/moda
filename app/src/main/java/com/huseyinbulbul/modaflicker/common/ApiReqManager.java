package com.huseyinbulbul.modaflicker.common;

/**
 * Created by Hüseyin on 27.8.2016.
 */
public class ApiReqManager {
    public static final String BASE_URL="https://api.flickr.com/services/rest/?&api_key=59422f7f13b7ae0db615200ff752ea90&method=";

    public static String getSearchUrl(){
        return BASE_URL+"flickr.photos.search&tags=moda&extras=url_m%2C+url_n%2Curl_l%2C+url_o&per_page=1000&page=1&format=json&nojsoncallback=1api_sig=9f61b3e7e0cbdfa22b7978c05094c368";
    }

    public static String getInfoUrl(String photoId){
        return BASE_URL+"flickr.photos.getInfo&photo_id="+photoId+"&format=json&nojsoncallback=1";
    }
}
